#ifndef TASK_H
#define TASK_H

#include <QWidget>
#include <QString>

namespace Ui {
class Task;
}

class Task : public QWidget
{
    Q_OBJECT

public:
    explicit Task(const QString& name, const QString& description, const QString& deadline, const QString& assigne, QFont font, QWidget *parent = 0);
    ~Task();

    void setName(const QString& name);
    void setDescription(const QString& name);
    void setDeadline(const QString& name);
    void setAssigne(const QString& name);
    QString name() const;
    bool isCompleted() const;
    void setFont(QFont font);

public slots:
    void rename();
    void changeDes();
    void changeAssign();
    void changeDeadline();
    void callChecked();
    void callDelete();

signals:
    void removed(Task* task);
    void statusChanged(Task* task);

private slots:
    void checked(bool checked);

private:
    Ui::Task *ui;
};

#endif // TASK_H
