<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="sv_SE">
<context>
    <name>MainWindow</name>
    <message>
        <location filename="MainWindow.ui" line="14"/>
        <source>Todo</source>
        <translation>AttGöra</translation>
    </message>
    <message>
        <location filename="MainWindow.ui" line="23"/>
        <source>Status: 0 todo / 0 done</source>
        <translation>Status 0 att göra / 0 färdiga</translation>
    </message>
    <message>
        <location filename="MainWindow.ui" line="43"/>
        <location filename="MainWindow.cpp" line="34"/>
        <source>Add task</source>
        <translation>Lägg till Uppgift</translation>
    </message>
    <message>
        <location filename="MainWindow.ui" line="89"/>
        <source>Change font</source>
        <translation>Ändra teckensnitt</translation>
    </message>
    <message>
        <location filename="MainWindow.ui" line="109"/>
        <source>Translate</source>
        <translation>Översätt</translation>
    </message>
    <message>
        <location filename="MainWindow.ui" line="149"/>
        <source>All Tasks Done</source>
        <translation>Alla Uppgifter klara</translation>
    </message>
    <message>
        <location filename="MainWindow.ui" line="172"/>
        <source>Delete All</source>
        <translation>Ta bort allt</translation>
    </message>
    <message>
        <location filename="MainWindow.cpp" line="35"/>
        <source>Task name</source>
        <translation>Uppgifts namn</translation>
    </message>
    <message>
        <location filename="MainWindow.cpp" line="36"/>
        <source>Untitled task</source>
        <translation>Namnlös uppgift</translation>
    </message>
    <message>
        <location filename="MainWindow.cpp" line="37"/>
        <source>Add description</source>
        <translation>Lägg till beskrivning</translation>
    </message>
    <message>
        <location filename="MainWindow.cpp" line="38"/>
        <source>Task description</source>
        <translation>Uppgifts beskrivning</translation>
    </message>
    <message>
        <location filename="MainWindow.cpp" line="40"/>
        <source>Add deadline</source>
        <translation>Lägg till tidsgräns</translation>
    </message>
    <message>
        <location filename="MainWindow.cpp" line="41"/>
        <source>Task deadline</source>
        <translation>Uppgifts tidsgräns</translation>
    </message>
    <message>
        <location filename="MainWindow.cpp" line="42"/>
        <source>01-12-2022</source>
        <translation></translation>
    </message>
    <message>
        <location filename="MainWindow.cpp" line="43"/>
        <source>Add assigne</source>
        <translation>Lägg till tilldelare</translation>
    </message>
    <message>
        <location filename="MainWindow.cpp" line="44"/>
        <source>Task assigne</source>
        <translation>Uppgifts tilldelare</translation>
    </message>
    <message>
        <location filename="MainWindow.cpp" line="45"/>
        <source>Unnamned assigne</source>
        <translation>ickenamngiven tilldelare</translation>
    </message>
</context>
<context>
    <name>Task</name>
    <message>
        <location filename="Task.ui" line="14"/>
        <source>Form</source>
        <translation>Form</translation>
    </message>
    <message>
        <location filename="Task.ui" line="22"/>
        <source>Task</source>
        <translation>Uppgift</translation>
    </message>
    <message>
        <location filename="Task.ui" line="28"/>
        <source>Name</source>
        <translation>Namn</translation>
    </message>
    <message>
        <location filename="Task.ui" line="34"/>
        <source>Buy Milk</source>
        <translation>Köp Mjölk</translation>
    </message>
    <message>
        <location filename="Task.ui" line="54"/>
        <location filename="Task.ui" line="97"/>
        <location filename="Task.ui" line="133"/>
        <location filename="Task.ui" line="169"/>
        <source>Edit</source>
        <translation>Ändra</translation>
    </message>
    <message>
        <location filename="Task.ui" line="61"/>
        <source>Remove</source>
        <translation>Ta bort</translation>
    </message>
    <message>
        <location filename="Task.ui" line="71"/>
        <source>Description</source>
        <translation>Beskrivning</translation>
    </message>
    <message>
        <location filename="Task.ui" line="77"/>
        <location filename="Task.ui" line="113"/>
        <location filename="Task.ui" line="149"/>
        <source>TextLabel</source>
        <translation>TextEtikett</translation>
    </message>
    <message>
        <location filename="Task.ui" line="107"/>
        <source>Deadline</source>
        <translation>Tidsgräns</translation>
    </message>
    <message>
        <location filename="Task.ui" line="143"/>
        <source>Assigne</source>
        <translation>Tilldelare</translation>
    </message>
    <message>
        <location filename="Task.cpp" line="85"/>
        <source>Edit task</source>
        <translation>Ändra Uppgift</translation>
    </message>
    <message>
        <location filename="Task.cpp" line="86"/>
        <source>Task name</source>
        <translation>Uppgifts namn</translation>
    </message>
    <message>
        <location filename="Task.cpp" line="96"/>
        <source>Edit description</source>
        <translation>Ändra beskrivning</translation>
    </message>
    <message>
        <location filename="Task.cpp" line="97"/>
        <source>Task description</source>
        <translation>Uppgifts beskrivning</translation>
    </message>
    <message>
        <location filename="Task.cpp" line="105"/>
        <source>Edit assigne</source>
        <translation>Ändra tilldelare</translation>
    </message>
    <message>
        <location filename="Task.cpp" line="106"/>
        <source>Task assigne</source>
        <translation>Uppgifts tilldelare</translation>
    </message>
    <message>
        <location filename="Task.cpp" line="116"/>
        <source>Edit deadline</source>
        <translation>Ändra tidsgräns</translation>
    </message>
    <message>
        <location filename="Task.cpp" line="117"/>
        <source>Task deadline</source>
        <translation>Uppgifts tidsgräns</translation>
    </message>
</context>
</TS>
