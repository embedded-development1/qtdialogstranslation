/********************************************************************************
** Form generated from reading UI file 'MainWindow.ui'
**
** Created by: Qt User Interface Compiler version 6.3.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QScrollArea>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralWidget;
    QVBoxLayout *verticalLayout;
    QHBoxLayout *toolbarLayout;
    QLabel *statusLabel;
    QSpacerItem *horizontalSpacer;
    QPushButton *addTaskButton;
    QScrollArea *scrollArea;
    QWidget *scrollAreaWidgetContents_2;
    QVBoxLayout *verticalLayout_2;
    QVBoxLayout *tasksLayout;
    QHBoxLayout *EditLayout;
    QSpacerItem *horizontalSpacer_2;
    QPushButton *ChangeFontButton;
    QSpacerItem *horizontalSpacer_4;
    QPushButton *TranslateButton;
    QSpacerItem *horizontalSpacer_3;
    QHBoxLayout *ClearLayout;
    QSpacerItem *horizontalSpacer_5;
    QPushButton *TasksDoneButton;
    QSpacerItem *horizontalSpacer_6;
    QPushButton *DeleteAllButton;
    QSpacerItem *horizontalSpacer_7;
    QSpacerItem *verticalSpacer;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(400, 300);
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        verticalLayout = new QVBoxLayout(centralWidget);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        toolbarLayout = new QHBoxLayout();
        toolbarLayout->setSpacing(6);
        toolbarLayout->setObjectName(QString::fromUtf8("toolbarLayout"));
        statusLabel = new QLabel(centralWidget);
        statusLabel->setObjectName(QString::fromUtf8("statusLabel"));

        toolbarLayout->addWidget(statusLabel);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        toolbarLayout->addItem(horizontalSpacer);

        addTaskButton = new QPushButton(centralWidget);
        addTaskButton->setObjectName(QString::fromUtf8("addTaskButton"));

        toolbarLayout->addWidget(addTaskButton);


        verticalLayout->addLayout(toolbarLayout);

        scrollArea = new QScrollArea(centralWidget);
        scrollArea->setObjectName(QString::fromUtf8("scrollArea"));
        scrollArea->setWidgetResizable(true);
        scrollAreaWidgetContents_2 = new QWidget();
        scrollAreaWidgetContents_2->setObjectName(QString::fromUtf8("scrollAreaWidgetContents_2"));
        scrollAreaWidgetContents_2->setGeometry(QRect(0, 0, 386, 164));
        verticalLayout_2 = new QVBoxLayout(scrollAreaWidgetContents_2);
        verticalLayout_2->setSpacing(6);
        verticalLayout_2->setContentsMargins(11, 11, 11, 11);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        tasksLayout = new QVBoxLayout();
        tasksLayout->setSpacing(6);
        tasksLayout->setObjectName(QString::fromUtf8("tasksLayout"));

        verticalLayout_2->addLayout(tasksLayout);

        scrollArea->setWidget(scrollAreaWidgetContents_2);

        verticalLayout->addWidget(scrollArea);

        EditLayout = new QHBoxLayout();
        EditLayout->setSpacing(6);
        EditLayout->setObjectName(QString::fromUtf8("EditLayout"));
        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        EditLayout->addItem(horizontalSpacer_2);

        ChangeFontButton = new QPushButton(centralWidget);
        ChangeFontButton->setObjectName(QString::fromUtf8("ChangeFontButton"));

        EditLayout->addWidget(ChangeFontButton);

        horizontalSpacer_4 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        EditLayout->addItem(horizontalSpacer_4);

        TranslateButton = new QPushButton(centralWidget);
        TranslateButton->setObjectName(QString::fromUtf8("TranslateButton"));

        EditLayout->addWidget(TranslateButton);

        horizontalSpacer_3 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        EditLayout->addItem(horizontalSpacer_3);


        verticalLayout->addLayout(EditLayout);

        ClearLayout = new QHBoxLayout();
        ClearLayout->setSpacing(6);
        ClearLayout->setObjectName(QString::fromUtf8("ClearLayout"));
        horizontalSpacer_5 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        ClearLayout->addItem(horizontalSpacer_5);

        TasksDoneButton = new QPushButton(centralWidget);
        TasksDoneButton->setObjectName(QString::fromUtf8("TasksDoneButton"));
        TasksDoneButton->setEnabled(false);

        ClearLayout->addWidget(TasksDoneButton);

        horizontalSpacer_6 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        ClearLayout->addItem(horizontalSpacer_6);

        DeleteAllButton = new QPushButton(centralWidget);
        DeleteAllButton->setObjectName(QString::fromUtf8("DeleteAllButton"));
        DeleteAllButton->setEnabled(false);

        ClearLayout->addWidget(DeleteAllButton);

        horizontalSpacer_7 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        ClearLayout->addItem(horizontalSpacer_7);


        verticalLayout->addLayout(ClearLayout);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer);

        verticalLayout->setStretch(1, 1);
        MainWindow->setCentralWidget(centralWidget);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QCoreApplication::translate("MainWindow", "Todo", nullptr));
        statusLabel->setText(QCoreApplication::translate("MainWindow", "Status: 0 todo / 0 done", nullptr));
        addTaskButton->setText(QCoreApplication::translate("MainWindow", "Add task", nullptr));
        ChangeFontButton->setText(QCoreApplication::translate("MainWindow", "Change font", nullptr));
        TranslateButton->setText(QCoreApplication::translate("MainWindow", "Translate", nullptr));
        TasksDoneButton->setText(QCoreApplication::translate("MainWindow", "All Tasks Done", nullptr));
        DeleteAllButton->setText(QCoreApplication::translate("MainWindow", "Delete All", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
