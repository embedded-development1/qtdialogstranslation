/********************************************************************************
** Form generated from reading UI file 'Task.ui'
**
** Created by: Qt User Interface Compiler version 6.3.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_TASK_H
#define UI_TASK_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Task
{
public:
    QHBoxLayout *horizontalLayout;
    QVBoxLayout *verticalLayout;
    QGroupBox *groupBox_2;
    QVBoxLayout *verticalLayout_2;
    QGroupBox *groupBoxName;
    QHBoxLayout *horizontalLayout_7;
    QCheckBox *checkbox;
    QSpacerItem *horizontalSpacer;
    QPushButton *editButton;
    QPushButton *removeButton;
    QGroupBox *groupBoxDesc;
    QHBoxLayout *horizontalLayout_2;
    QLabel *DescriptionText;
    QSpacerItem *horizontalSpacer_2;
    QPushButton *editButton_Des;
    QGroupBox *groupBox;
    QHBoxLayout *horizontalLayout_3;
    QLabel *DeadlineText;
    QSpacerItem *horizontalSpacer_3;
    QPushButton *editButton_Deadline;
    QGroupBox *groupBoxAssigne;
    QHBoxLayout *horizontalLayout_4;
    QLabel *AssigneText;
    QSpacerItem *horizontalSpacer_4;
    QPushButton *editButton_Assign;

    void setupUi(QWidget *Task)
    {
        if (Task->objectName().isEmpty())
            Task->setObjectName(QString::fromUtf8("Task"));
        Task->resize(400, 300);
        horizontalLayout = new QHBoxLayout(Task);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        verticalLayout = new QVBoxLayout();
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        groupBox_2 = new QGroupBox(Task);
        groupBox_2->setObjectName(QString::fromUtf8("groupBox_2"));
        verticalLayout_2 = new QVBoxLayout(groupBox_2);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        groupBoxName = new QGroupBox(groupBox_2);
        groupBoxName->setObjectName(QString::fromUtf8("groupBoxName"));
        horizontalLayout_7 = new QHBoxLayout(groupBoxName);
        horizontalLayout_7->setObjectName(QString::fromUtf8("horizontalLayout_7"));
        checkbox = new QCheckBox(groupBoxName);
        checkbox->setObjectName(QString::fromUtf8("checkbox"));

        horizontalLayout_7->addWidget(checkbox);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_7->addItem(horizontalSpacer);

        editButton = new QPushButton(groupBoxName);
        editButton->setObjectName(QString::fromUtf8("editButton"));

        horizontalLayout_7->addWidget(editButton);

        removeButton = new QPushButton(groupBoxName);
        removeButton->setObjectName(QString::fromUtf8("removeButton"));

        horizontalLayout_7->addWidget(removeButton);


        verticalLayout_2->addWidget(groupBoxName);

        groupBoxDesc = new QGroupBox(groupBox_2);
        groupBoxDesc->setObjectName(QString::fromUtf8("groupBoxDesc"));
        horizontalLayout_2 = new QHBoxLayout(groupBoxDesc);
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        DescriptionText = new QLabel(groupBoxDesc);
        DescriptionText->setObjectName(QString::fromUtf8("DescriptionText"));

        horizontalLayout_2->addWidget(DescriptionText);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_2);

        editButton_Des = new QPushButton(groupBoxDesc);
        editButton_Des->setObjectName(QString::fromUtf8("editButton_Des"));

        horizontalLayout_2->addWidget(editButton_Des);


        verticalLayout_2->addWidget(groupBoxDesc);

        groupBox = new QGroupBox(groupBox_2);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        horizontalLayout_3 = new QHBoxLayout(groupBox);
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        DeadlineText = new QLabel(groupBox);
        DeadlineText->setObjectName(QString::fromUtf8("DeadlineText"));

        horizontalLayout_3->addWidget(DeadlineText);

        horizontalSpacer_3 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_3);

        editButton_Deadline = new QPushButton(groupBox);
        editButton_Deadline->setObjectName(QString::fromUtf8("editButton_Deadline"));

        horizontalLayout_3->addWidget(editButton_Deadline);


        verticalLayout_2->addWidget(groupBox);

        groupBoxAssigne = new QGroupBox(groupBox_2);
        groupBoxAssigne->setObjectName(QString::fromUtf8("groupBoxAssigne"));
        horizontalLayout_4 = new QHBoxLayout(groupBoxAssigne);
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        AssigneText = new QLabel(groupBoxAssigne);
        AssigneText->setObjectName(QString::fromUtf8("AssigneText"));

        horizontalLayout_4->addWidget(AssigneText);

        horizontalSpacer_4 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_4->addItem(horizontalSpacer_4);

        editButton_Assign = new QPushButton(groupBoxAssigne);
        editButton_Assign->setObjectName(QString::fromUtf8("editButton_Assign"));

        horizontalLayout_4->addWidget(editButton_Assign);


        verticalLayout_2->addWidget(groupBoxAssigne);


        verticalLayout->addWidget(groupBox_2);


        horizontalLayout->addLayout(verticalLayout);


        retranslateUi(Task);

        QMetaObject::connectSlotsByName(Task);
    } // setupUi

    void retranslateUi(QWidget *Task)
    {
        Task->setWindowTitle(QCoreApplication::translate("Task", "Form", nullptr));
        groupBox_2->setTitle(QCoreApplication::translate("Task", "Task", nullptr));
        groupBoxName->setTitle(QCoreApplication::translate("Task", "Name", nullptr));
        checkbox->setText(QCoreApplication::translate("Task", "Buy Milk", nullptr));
        editButton->setText(QCoreApplication::translate("Task", "Edit", nullptr));
        removeButton->setText(QCoreApplication::translate("Task", "Remove", nullptr));
        groupBoxDesc->setTitle(QCoreApplication::translate("Task", "Description", nullptr));
        DescriptionText->setText(QCoreApplication::translate("Task", "TextLabel", nullptr));
        editButton_Des->setText(QCoreApplication::translate("Task", "Edit", nullptr));
        groupBox->setTitle(QCoreApplication::translate("Task", "Deadline", nullptr));
        DeadlineText->setText(QCoreApplication::translate("Task", "TextLabel", nullptr));
        editButton_Deadline->setText(QCoreApplication::translate("Task", "Edit", nullptr));
        groupBoxAssigne->setTitle(QCoreApplication::translate("Task", "Assigne", nullptr));
        AssigneText->setText(QCoreApplication::translate("Task", "TextLabel", nullptr));
        editButton_Assign->setText(QCoreApplication::translate("Task", "Edit", nullptr));
    } // retranslateUi

};

namespace Ui {
    class Task: public Ui_Task {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_TASK_H
