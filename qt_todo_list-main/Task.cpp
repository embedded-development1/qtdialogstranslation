#include "Task.h"
#include "ui_Task.h"

#include <QInputDialog>
#include <QDebug>

Task::Task(const QString& name, const QString& description, const QString& deadline, const QString& assigne, QFont font, QWidget *parent) :
        QWidget(parent),
        ui(new Ui::Task)
{
    ui->setupUi(this);
    setName(name);
    setDescription(description);
    setDeadline(deadline);
    setAssigne(assigne);
    setFont(font);

    connect(ui->editButton, &QPushButton::clicked, this, &Task::rename);
    connect(ui->editButton_Des, &QPushButton::clicked, this, &Task::changeDes);
    connect(ui->editButton_Deadline, &QPushButton::clicked, this, &Task::changeDeadline);
    connect(ui->editButton_Assign, &QPushButton::clicked, this, &Task::changeAssign);
    connect(ui->removeButton, &QPushButton::clicked, [this] {
        emit removed(this);
    });
    connect(ui->checkbox, &QCheckBox::toggled, this, &Task::checked);
}

Task::~Task()
{
    qDebug() << "~Task() called";
    delete ui;
}

void Task::setName(const QString& name)
{
    ui->checkbox->setText(name);
}

void Task::setDescription(const QString &name)
{
    ui->DescriptionText->setText(name);
}

void Task::setDeadline(const QString &name)
{
    ui->DeadlineText->setText(name);
}

void Task::setAssigne(const QString &name)
{
    ui->AssigneText->setText(name);
}

QString Task::name() const
{
    return ui->checkbox->text();
}

bool Task::isCompleted() const
{
    return ui->checkbox->isChecked();
}

void Task::setFont(QFont font)
{
    ui->groupBox_2->setFont(font);
    ui->groupBoxName->setFont(font);
    ui->groupBox->setFont(font);
    ui->groupBoxAssigne->setFont(font);
    ui->groupBoxDesc->setFont(font);
    ui->removeButton->setFont(font);
    ui->DescriptionText->setFont(font);
    ui->AssigneText->setFont(font);
    ui->DeadlineText->setFont(font);
    ui->checkbox->setFont(font);
    ui->editButton->setFont(font);
    ui->editButton_Assign->setFont(font);
    ui->editButton_Deadline->setFont(font);
    ui->editButton_Des->setFont(font);
}

void Task::rename()
{
    bool ok;
    QString value = QInputDialog::getText(this, tr("Edit task"),
                                          tr("Task name"), QLineEdit::Normal,
                                          this->name(), &ok);
    if (ok && !value.isEmpty()) {
        setName(value);
    }
}

void Task::changeDes()
{
    bool ok;
    QString text = QInputDialog::getMultiLineText(this, tr("Edit description"),
                                                  tr("Task description"), "", &ok);
    if (ok && !text.isEmpty())
        setDescription(text);
}

void Task::changeAssign()
{
    bool ok;
    QString value = QInputDialog::getText(this, tr("Edit assigne"),
                                          tr("Task assigne"), QLineEdit::Normal,
                                          this->name(), &ok);
    if (ok && !value.isEmpty()) {
        setAssigne(value);
    }
}

void Task::changeDeadline()
{
    bool ok;
    QString value = QInputDialog::getText(this, tr("Edit deadline"),
                                          tr("Task deadline"), QLineEdit::Normal,
                                          this->name(), &ok);
    if (ok && !value.isEmpty()) {
        setDeadline(value);
    }
}

void Task::callChecked()
{
    ui->checkbox->setChecked(true);
    checked(true);
}

void Task::callDelete()
{
    emit removed(this);
}

void Task::checked(bool checked)
{
    QFont font(ui->checkbox->font());
    font.setStrikeOut(checked);
    ui->checkbox->setFont(font);

    emit statusChanged(this);
}
