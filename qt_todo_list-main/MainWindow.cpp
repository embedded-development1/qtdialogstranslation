#include "MainWindow.h"
#include "ui_MainWindow.h"

#include <QDebug>
#include <QtWidgets>
#include <QInputDialog>
#include <QDateTimeEdit>
#include <QTranslator>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    mTasks()
{
    ui->setupUi(this);
    connect(ui->addTaskButton, &QPushButton::clicked, this, &MainWindow::addTask);
    connect(ui->ChangeFontButton, &QPushButton::clicked, this, &MainWindow::setFont);
    connect(ui->TasksDoneButton, &QPushButton::clicked, this, &MainWindow::clearAllTasks);
    connect(ui->DeleteAllButton, &QPushButton::clicked, this, &MainWindow::removeAllTasks);
    currentFont = QFont("Times", 9);
    updateStatus();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::addTask()
{
    bool ok;
    QString name = QInputDialog::getText(this, tr("Add task"),
                                         tr("Task name"), QLineEdit::Normal,
                                         tr("Untitled task"), &ok);
    QString description = QInputDialog::getMultiLineText(this, tr("Add description"),
                                                                            tr("Task description"), "", &ok);
    QString deadline = QInputDialog::getText(this, tr("Add deadline"),
                                         tr("Task deadline"), QLineEdit::Normal,
                                         tr("01-12-2022"), &ok);
    QString assigne = QInputDialog::getText(this, tr("Add assigne"),
                                         tr("Task assigne"), QLineEdit::Normal,
                                         tr("Unnamned assigne"), &ok);
    if (ok && !name.isEmpty() && !description.isEmpty() && !deadline.isEmpty() && !assigne.isEmpty()) {
        qDebug() << "Adding new task";
        Task* task = new Task(name, description, deadline, assigne, currentFont);
        connect(task, &Task::removed, this, &MainWindow::removeTask);
        connect(task, &Task::statusChanged, this, &MainWindow::taskStatusChanged);
        mTasks.append(task);
        ui->tasksLayout->addWidget(task);
        updateStatus();
    }
}

void MainWindow::setFont()
{
    const QString &text = ui->statusLabel->text();
    if (!text.isEmpty())
        currentFont.fromString(text);

    bool ok;
    QFont font = QFontDialog::getFont(&ok, currentFont, this, "Select Font");
    if (ok) {
        currentFont = font;
        ui->ChangeFontButton->setFont(currentFont);
        ui->TranslateButton->setFont(currentFont);
        ui->addTaskButton->setFont(currentFont);
        ui->statusLabel->setFont(currentFont);
        for (int var = 0; var < mTasks.size(); ++var) {
            mTasks[var]->setFont(currentFont);
        }
    }
}

void MainWindow::clearAllTasks()
{
    for (int var = 0; var < mTasks.size(); ++var) {
        mTasks[var]->callChecked();
        updateStatus();
    }
}

void MainWindow::removeAllTasks()
{
    for (int var = 0; var < mTasks.size(); ++var) {
        ui->tasksLayout->removeWidget(mTasks[var]);
        delete mTasks[var];
    }
    mTasks.clear();
    updateStatus();
}

void MainWindow::removeTask(Task* task)
{
    mTasks.removeOne(task);
    ui->tasksLayout->removeWidget(task);
    delete task;
    updateStatus();
}

void MainWindow::taskStatusChanged(Task* /*task*/)
{
    updateStatus();
}

void MainWindow::updateStatus()
{
    int completedCount = 0;
    for(auto t : mTasks)  {
        if (t->isCompleted()) {
            completedCount++;
        }
    }
    if(!mTasks.isEmpty()){
        ui->TasksDoneButton->setEnabled(true);
        ui->DeleteAllButton->setEnabled(true);
    }
    else{
        ui->TasksDoneButton->setEnabled(false);
        ui->DeleteAllButton->setEnabled(false);
    }
    int todoCount = mTasks.size() - completedCount;

    ui->statusLabel->setText(QString("Status: %1 todo / %2 completed")
                             .arg(todoCount)
                             .arg(completedCount));
}
